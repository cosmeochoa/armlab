
import matplotlib.pyplot as plt
import numpy as np
import math

def rad2epsilon(num_cells, radius):
	return (2.0*radius)/(1.0*num_cells)


def shiftList(sim_pts, shift):
	shifted_pts = [[None]*2 for x in range(len(sim_pts))]

	for i in range(len(sim_pts)):
		shifted_pts[i][0] = sim_pts[i][0] - shift
		shifted_pts[i][1] = sim_pts[i][1] - shift

	return shifted_pts

def shiftPoint(point_sim, shift):
	x_sim = point_sim[0]
	y_sim = point_sim[1]
	x_shift = x_sim - shift
	y_shift = y_sim - shift

	return (x_shift, y_shift)

def sim2worldList(sim_pts, epsilon):
	world_pts = [[None]*2 for x in range(len(sim_pts))]

	for i in range(len(sim_pts)):
		world_pts[i][0] = (sim_pts[i][0] - 0.5)*epsilon
		world_pts[i][1] = (sim_pts[i][1] - 0.5)*epsilon

	return world_pts

def sim2worldPoint(x_sim, y_sim, epsilon):
	x_world = (x_sim - 0.5)*epsilon
	y_world = (y_sim - 0.5)*epsilon

	return x_world, y_world

def world2simList(world_pts, epsilon):
	sim_pts = [[None]*2 for x in range(len(world_pts))]

	for i in range(len(sim_pts)):
		sim_pts[i][0] = (world_pts[i][0])/epsilon + 0.5
		sim_pts[i][1] = (world_pts[i][1])/epsilon + 0.5

	return world_pts

def world2simPoint(point_world, epsilon):
	x_world = point_world[0]
	y_world = point_world[1]
	x_sim = (x_world)/epsilon + 0.5
	y_sim = (y_world)/epsilon + 0.5

def findStart(maze_matrix):
	# Assumes solution begins at the top
	for j in range(len(maze_matrix)):
		count = 0;
		for i in range(len(maze_matrix[0])):
			if (maze_matrix[i][j][0] == 255 
			and maze_matrix[i][j][1] == 0 
			and maze_matrix[i][j][2] == 0):
				return i, j
				count += 1

def getNeighbors(idx, idy, max_idx, max_idy):
	
	neighbors = []

	if idx - 1 >= 0 and idy - 1 >= 0:
		neighbors.append((idx - 1, idy - 1))
	if idy -1 >= 0:
		neighbors.append((idx + 0, idy - 1))
	if idx + 1 < max_idx and idy - 1 >= 0:
		neighbors.append((idx + 1, idy - 1))
	if idx + 1 < max_idx:
		neighbors.append((idx + 1, idy + 0))
	if idx + 1 < max_idx and idy + 1 < max_idy:
		neighbors.append((idx + 1, idy + 1))
	if idy + 1 < max_idy:
		neighbors.append((idx + 0, idy + 1))
	if idx - 1 >= 0 and idy + 1 < max_idy:
		neighbors.append((idx - 1, idy + 1))
	if idx - 1 > 0:
		neighbors.append((idx - 1, idy + 0))

	return neighbors

def reorderNeighbors(neighbors, idx, idy, heading):
	ordered_neighbors = []
	heading = minimizeAngle(heading)
	phi = heading
	count = 0

	for neigh in neighbors:
		theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
		theta = minimizeAngle(theta)
		if theta == phi:
			ordered_neighbors.append(neigh)
			neighbors.remove(neigh)
			count += 1
			# print "Count:", count
			break

	phi = minimizeAngle(heading - 2*45)
	
	for neigh in neighbors:
		theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
		theta = minimizeAngle(theta)
		if theta == phi:
			ordered_neighbors.append(neigh)
			neighbors.remove(neigh)
			count += 1
			# print "Count:", count
			break

	phi = minimizeAngle(heading + 2*45)
	
	for neigh in neighbors:
		theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
		theta = minimizeAngle(theta)
		if theta == phi:
			ordered_neighbors.append(neigh)
			neighbors.remove(neigh)
			count += 1
			# print "Count:", count
			break

	phi = minimizeAngle(heading - 45)
	
	for neigh in neighbors:
		theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
		theta = minimizeAngle(theta)
		if theta == phi:
			ordered_neighbors.append(neigh)
			neighbors.remove(neigh)
			count += 1
			# print "Count:", count
			break

	phi = minimizeAngle(heading + 45)
	
	for neigh in neighbors:
		theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
		theta = minimizeAngle(theta)
		if theta == phi:
			ordered_neighbors.append(neigh)
			neighbors.remove(neigh)
			count += 1
			# print "Count:", count
			break

	# phi = minimizeAngle(heading - 3*45)
	
	# for neigh in neighbors:
	# 	theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
	# 	theta = minimizeAngle(theta)
	# 	if theta == phi:
	# 		ordered_neighbors.append(neigh)
	# 		neighbors.remove(neigh)
	# 		count += 1
	# 		# print "Count:", count
	# 		break

	# phi = minimizeAngle(heading + 3*45)
	
	# for neigh in neighbors:
	# 	theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
	# 	theta = minimizeAngle(theta)
	# 	if theta == phi:
	# 		ordered_neighbors.append(neigh)
	# 		neighbors.remove(neigh)
	# 		count += 1
	# 		# print "Count:", count
	# 		break

	# phi = minimizeAngle(heading + 4*45)
	
	# for neigh in neighbors:
	# 	theta = int((180/math.pi)*math.atan2(neigh[1] - idy, neigh[0] - idx))
	# 	theta = minimizeAngle(theta)
	# 	if theta == phi:
	# 		ordered_neighbors.append(neigh)
	# 		neighbors.remove(neigh)
	# 		count += 1
	# 		# print "Count:", count
	# 		break

	return ordered_neighbors

def minimizeAngle(angle):

	while angle >= 180:
		angle -= 360
	while angle < -180:
		angle += 360

	return angle


def checkCondition(idx, idy, maze_matrix, tol):
	return (maze_matrix[idx][idy][0] == 255 
			and maze_matrix[idx][idy][1] < tol 
			and maze_matrix[idx][idy][2] < tol)

def getSolution(start_idx, start_idy, maze_matrix):
	solution = []

	max_idx = len(maze_matrix)
	max_idy = len(maze_matrix[0])
	
	# Append the starting point
	flag_found_goal = False
	old_idx = -1
	old_idy = -1
	curr_idx = start_idx
	curr_idy = start_idy
	heading = 90

	while not flag_found_goal:

		print old_idx, curr_idx, old_idy, curr_idy

		# Check whether we can continue along a path
		if old_idx == curr_idx and old_idy == curr_idy:
			flag_found_goal = True
			print "Found Goal!"
			break

		# Grab the 8-connected neearest neighbors
		neighbors = getNeighbors(curr_idx, curr_idy, max_idx, max_idy)
		# print heading, neighbors
		neighbors = reorderNeighbors(neighbors, curr_idx, curr_idy, heading)
		print heading, curr_idx, curr_idy, neighbors

		count = 8

		tol = 0
		flag_found_neigh = 0
		while (not flag_found_neigh):
			tol += 1
			count = 0
			for neigh in neighbors:

				# If neighbor is already in solution ignore it
				if neigh in solution:
					count += 1
					continue
				else:
					# Check that the neighbor in question is part of solution path
					print neigh[0], neigh[1], (maze_matrix[neigh[0]][neigh[1]][0],
						maze_matrix[neigh[0]][neigh[1]][1],
						maze_matrix[neigh[0]][neigh[1]][2])
					if checkCondition(neigh[0], neigh[1], maze_matrix, tol):
						old_idx = curr_idx
						old_idy = curr_idy
						curr_idx = neigh[0]
						curr_idy = neigh[1]
						heading = int((180/math.pi)*math.atan2(curr_idy - old_idy, curr_idx - old_idx))
						heading = minimizeAngle(heading)
						print "In:", neigh
						solution.append(neigh)
						flag_found_neigh = 1
						break

				count += 1

			# print solution
			print tol, count, len(neighbors)
			if count == len(neighbors):
				if tol > 68:
					flag_found_goal = True
					break


	# for i in range(len(maze_matrix)):
	# 	count = 0;
	# 	for j in range(len(maze_matrix[0])):
	# 		if (maze_matrix[i][j][0] == 255 
	# 		and maze_matrix[i][j][1] == 0 
	# 		and maze_matrix[i][j][2] == 0):
	# 			solution.append([i,j])
	# 			count += 1

	return solution

def getMaze(maze_name, size_x, size_y):
	
	# Define directory and file name
	file_directory = "Scenes/"
	file_name = maze_name + 'R.txt'

	# Initialize maze matrix
	maze_matrix = [[[None]*3 for y in range(size_y)] for x in range(size_x)]

	# Add R-Value
	with open(file_directory + file_name, 'r') as f:
		j = 0
		# Read line from text file
		for line in f:
			# Parse line with comma delimeter
			sline = line.split(',')

			# Convert string data to integers
			for i in range(len(sline)):
				maze_matrix[i][j][0] = int(sline[i])

			j += 1

	file_name = maze_name + 'G.txt'

	# Add G-Value
	with open(file_directory + file_name, 'r') as f:
		j = 0
		# Read line from text file
		for line in f:
			# Parse line with comma delimeter
			sline = line.split(',')

			# Convert string data to integers
			for i in range(len(sline)):
				maze_matrix[i][j][1] = int(sline[i])

			j += 1

	file_name = maze_name + 'B.txt'

	# Add B-Value
	with open(file_directory + file_name, 'r') as f:
		j = 0
		# Read line from text file
		for line in f:
			# Parse line with comma delimeter
			sline = line.split(',')

			# Convert string data to integers
			for i in range(len(sline)):
				maze_matrix[i][j][2] = int(sline[i])

			j += 1

	return maze_matrix

def decodeMaze(maze_matrix):

	decode_matrix = [[0]*len(maze_matrix[1]) for x in range(len(maze_matrix))]
	
	for i in range(len(maze_matrix)):
		count = 0;
		for j in range(len(maze_matrix[0])):
			if (maze_matrix[i][j][0] == 0 
			and maze_matrix[i][j][1] == 0 
			and maze_matrix[i][j][2] == 0):
				decode_matrix[i][j] = 1
				count += 1

	# print len(decode_matrix), len(decode_matrix[1])
	return decode_matrix
