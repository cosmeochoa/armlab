import cv2
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.w = int(capture.get(3))
        self.h = int(capture.get(4))
        self.currentFrame = np.array([])
        self.grayFrame = np.array([])
        self.camera_matrix = None
        self.dist_coefs = None
        self.new_camera_matrix = None
        self.state = 0
        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """

        self.aff_npoints = 5                                    # Change!
        self.real_coord = np.float32([[0.0, 0.0],[295, -310],[-310,-310], [-302, 295], [303, 290]])

        #self.aff_npoints = 3                                     # Change!
        #self.real_coord = np.float32([[302.4, 302.4], [302.4,-302.4], [-302.4,-302.4]])

        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])    
        self.square_coord = np.float32(np.zeros((2*self.aff_npoints,6)))  
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32(np.zeros((2,3)))
        self.pixel_obs_loc = []

    def loadObstacles(self, obstacles):
        self.pixel_obs_loc = obstacles
    
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret==True):
            self.currentFrame = cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)
            #self.grayFrame = cv2.cvtColor(frame, cv2.COLOR_GB2GRAY)
            if self.state == 1:
                #print "Undistortion Image"
                self.currentFrame = cv2.undistort(self.currentFrame, self.camera_matrix, self.dist_coefs, None, self.new_camera_matrix)
                #self.grayFrame = cv2.undistort(self.grayFrame, self.camera_matrix, self.dist_coefs, None, self.new_camera_matrix)
        for i in range(len(self.pixel_obs_loc)):
            cv2.circle(self.currentFrame, (2*self.pixel_obs_loc[i][0], 2*self.pixel_obs_loc[i][1]), 5, (0,0,255), -1)

    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """

        self.camera_matrix = np.loadtxt("CameraMatrix.txt")
        self.dist_coefs = np.loadtxt("DistortionCoefficients.txt")
        self.new_camera_matrix, roi = cv2.getOptimalNewCameraMatrix(self.camera_matrix,self.dist_coefs,(self.w,self.h),1,(self.w,self.h))

        self.state = 1

        # ret, frame = self.capture.read()
        # rgb_frame = cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2RGB)
        # undistorted = cv2.undistort(rgb_frame, camera_matrix, dist_coefs, None, new_camera_matrix)
        # cv2.imshow('camera', undistorted)

        # pass

