"""
control_station.py

Used to view the position of the arm using an overhead problem and approximate a desired location in the x-y plane with the use of the mouse.
It addition, the GUI provides the functionality to load a predefined camera calibration, perform an affine calibration, use a teach and repeat algorithm,
smooth a path of captured waypoints, execute captured set of wayponints, define a target template, locate templates viewed by the camera, and execute a
an autonomous path to reach all the designated targets.

The GUI also provides a visual feedback of the current joint space angles for each arm joint (base, shoulder, elbow, wrist) and end effector position in 
the world coordinate frame

Functions: 
Gui: -> Class that defines the GUI used to control the robot arm and read any feedback.
play(self) -> Called continuously by GUI to update the video feed from the overhead camera.
slider_change(self) -> Used to update torque, speed, and joint angles of the arm directly.
clamp(self): -> Used to limit the commanded joint angles within the physical limits of each joint.
mousePressEvent(self): -> Used to record anytime the mouse was clicked while on the GUI.
affine_cal(self): -> Used to initiate affine calibration process.
load_camera_cal(self): -> Used to load camera matrix and distortion coefficients from cameral calibration.
def record_data(self, data, filename): -> Used to save data to text files.
def tr_initialize(self): Used to initiate teach and repeat mode for the robot arm.
def tr_add_waypoint(self): Used to save a waypoint when in teach and repeat mode.
def tr_smooth_path(self): Used to smooth a recorded set of waypoints.
def tr_playback(self): Used to repeat a taught set of waypoints or its smooth version.
def def_template(self): Used to define a template of object to be found by the camera.
def template_match(self): Matches the template throughout the camera frame and keeps track of all objects found.
def exec_path(self): Executes the fun path, if no template matching was performed, or the planned path to reach all the target objects.
"""

import sys
import cv2
import time
import numpy as np
import math
import csv
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm

from video import Video

from maze import *

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510
 
class Gui(QtGui.QMainWindow):
    """ 
    Main GUI Class
    It contains the main function and interfaces between 
    the GUI and functions
    """
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        """ Control Panel State"""
        self.state = "None"

        """ Teach and Repeat Waypoints"""
        self.teach_waypoints = []
        self.teach_waypoint_counter = 0
        self.waypoint_times = []
        self.smooth_waypoint = []

        """ Cubic Waypoints"""
        self.cubic_waypoints = []

        """ Waypoint Threshold"""
        self.teach_treshold = 0.1

        """ Auto Waypoints"""
        self.auto_teach_waypoints = False

        """ Smooth Path"""
        self.teach_smooth = False

        """ Cubic Path """
        self.cubic = False

        """ Camera Waypoints"""
        self.camera_waypoints = []

        """ Template Matching """
        self.pixel_obs_matrix = []
        self.pixel_obs_matrix_gray = []
        self.obs_size_x = 0 # the obstacle size_x in "Pixel Mouse Coordinates"
        self.obs_size_y = 0 # the obstacle size_y in "Pixel Moyuse Coordinates"
        self.pixel_obs_location_list = []
        self.affine_obs_location_list = []
        self.board_size_x = 200 # the board size_x in "Pixel Mouse Coordinates"
        self.board_size_y = 200 # the board size_y in "Pixel Mouse Coordinates"
        self.template_matching_threshold = 1500
        self.center_of_board = []
        self.board_top_left = []
        self.board_bottom_left = []
        self.template_record = False
        self.template_mouse_click = 0
        self.template_coords = []
        self.num_of_obs = 0
        self.min_diff = 100000
        self.pixel_frame = []
        self.real_obs_loc = []
        self.real_num_of_obs = 0
        self.pixel_obs_loc = []
        self.template_status = False

        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))

        """ Other Variables """
        self.last_click = np.float32([0,0])

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """ 
        Video Function 
        Creates a timer and calls play() function 
        according to the given time delay (27mm) 
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)
       
        """ 
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time 
        """  
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()

        """ Choose to use OpenCV Affine Matrix Calculation"""
        self.useOpenCVAffine = False

        """ 
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI 
        """ 
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)

        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change() 
        
        """ Connect Buttons to Functions """
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)

    def play(self):
        """ 
        Play Funtion
        Continuously called by GUI 
        """

        """ Renders the Video Frame """
        # try:
        #     self.video.captureNextFrame()
        #     self.ui.videoFrame.setPixmap(
        #         self.video.convertFrame())
        #     self.ui.videoFrame.setScaledContents(True)
        # except TypeError:
        #     print "No frame"
        
        """ 
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels 
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))
        self.ui.rdoutX.setText(str(self.rex.forward_x))
        self.ui.rdoutY.setText(str(self.rex.forward_y))
        self.ui.rdoutZ.setText(str(self.rex.forward_z))
        self.ui.rdoutT.setText(str(90-self.rex.joint_angles_fb[1]*R2D-self.rex.joint_angles_fb[2]*R2D-self.rex.joint_angles_fb[3]*R2D))

        """ 
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates 
        """    
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        #print x, y
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
            #print "Out of bounds!"
        else:
            #print "Inside bounds!"
            x = x - MIN_X
            y = y - MIN_Y

            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))

            if (self.video.aff_flag == 2):
                pixel_point = [[x],[y],[1]]
                world_point = np.dot(self.video.aff_matrix, pixel_point)

                x_w = world_point[0][0]
                y_w = world_point[1][0]
                """ TO DO Here is where affine calibration must be used """
                self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" % (x_w,y_w))
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """ 
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """ 
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))

        if self.state == "Teach" and self.auto_teach_waypoints == True:
            base_angle = self.rex.joint_angles_fb[0]
            shoulder_angle = self.rex.joint_angles_fb[1]
            elbow_angle = self.rex.joint_angles_fb[2]
            wrist_angle = self.rex.joint_angles_fb[3]

            # Add joint angles to waypoint list
            waypoint = [base_angle, shoulder_angle, elbow_angle, wrist_angle]
            self.teach_waypoints.append(waypoint)

    def slider_change(self):
        """ 
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position 
        TO DO: Implement for the other sliders
        """

        # If teaching, arm will ignore any slider input.
        print self.state
        
        if self.state == "Teach":
            pass

        else:
            # Update joint angle values based on slider values
            self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
            self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
            self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
            self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))

            # Update torque slider input value
            self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
            self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0

            # Update speed slider input value
            self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")
            #self.rex.speed = self.ui.sldrSpeed.value()/100.0

            # Code altered to allow possibility of giving each a joint different speed
            # By default it gives all joints the same speed input
            uispeed = self.ui.sldrSpeed.value()/100.0
            self.rex.speed = uispeed

            # Update arm joint angles and command position
            self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
            self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
            self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
            self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R

            # Command arm to given slider position
            self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """ 
        Function used to record mouse click positions for 
        affine calibration 
        """
 
        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)): return

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y

        if self.template_record == True:
            self.template_coords.append([x - MIN_X,y - MIN_Y])
            self.template_mouse_click += 1

            if self.template_mouse_click > 1:
                print "There exists already two points"
                self.template_record = False
                self.template_mouse_click = 0

                # getting the pixelwise grayscalse data
                self.pixel_frame = cv2.cvtColor(self.video.currentFrame, cv2.COLOR_BGR2GRAY)
                #print "Pixel Frame:", self.pixel_frame
                cv2.imwrite('gray_frame.png', self.pixel_frame)

                # the board size in "Pixel Mouse Coordinates" in GUI of control_station = 200 * 200
                # the pixelwise board size in "Pixel Mouse Coordinates" (since each coordinate has two pixels) = 400 * 400 
                # the actual board size = 600 * 600

                # Below coordinates are all in "Pixel Mouse Coordinates"
                # Step1. getting two points of the obstacle
                
                obs_x1 = self.template_coords[0][0]
                obs_y1 = self.template_coords[0][1]
                obs_x2 = self.template_coords[1][0]
                obs_y2 = self.template_coords[1][1]
                print "Top left corner of the obstacle (%d, %d)" %(obs_x1, obs_y1)
                print "Bottom right corner of the obstacle (%d, %d)" %(obs_x2, obs_y2)

                self.obs_size_x = abs(obs_x2 - obs_x1) + 1
                self.obs_size_x = int(self.obs_size_x)
                self.obs_size_y = abs(obs_y2 - obs_y1) + 1
                self.obs_size_y = int(self.obs_size_y)

                pixel_obs_rowsize = self.obs_size_x * 2
                pixel_obs_colsize = self.obs_size_y * 2

                self.pixel_obs_matrix = np.zeros([pixel_obs_colsize, pixel_obs_rowsize])

                # Step2. getting RGB value of each pixels of obstacle matrix from self.video.currentFrame
                for i in range(pixel_obs_rowsize):
                    # creating empty matrix of obstacle matrix
                    # pixel_obs_rowvector = np.empty((pixel_obs_colsize, 3))
                    for j in range(pixel_obs_colsize):
                        #for k in range(3):
                            #pixel_obs_rowvector[j][k] = pixel_frame[obs_y1*2+j][obs_x1*2+i][k]
                        self.pixel_obs_matrix[j][i] = self.pixel_frame[obs_y1*2+j][obs_x1*2+i]
                    #self.pixel_obs_matrix.append(pixel_obs_rowvector)

                print "Print Obstacle Template:", self.pixel_obs_matrix
                cv2.imwrite('gray_obstacles.png', self.pixel_obs_matrix)


       
        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]
            
            if self.useOpenCVAffine == False:
                self.video.square_coord[2*self.video.mouse_click_id] = [(x-MIN_X),(y-MIN_Y),1,0,0,0]
                self.video.square_coord[2*self.video.mouse_click_id + 1] = [0,0,0,(x-MIN_X),(y-MIN_Y),1]
 
            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                      %(self.video.mouse_click_id + 1))

            """ 
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                # Note: Turn on/off self.useOpenCVAffine to choose whether to use the default OpenCV Method or the Least Squares Method 
                if self.useOpenCVAffine == True:
                    """ 
                    Update status of calibration flag and number of mouse
                    clicks
                    """
                    self.video.aff_flag = 2
                    self.video.mouse_click_id = 0

                    self.center_of_board = self.video.mouse_coord[0]
                    self.board_bottom_left = self.video.mouse_coord[1]
                    self.board_top_left = self.video.mouse_coord[2]
                    
                    """ Perform affine calibration with OpenCV """
                    self.video.aff_matrix = cv2.getAffineTransform(
                                            self.video.mouse_coord,
                                            self.video.real_coord)
                
                    """ Updates Status Label to inform calibration is done """ 
                    self.ui.rdoutStatus.setText("Waiting for input")

                    """ 
                    Uncomment to gether affine calibration matrix numbers 
                    on terminal
                    """ 
                    print self.center_of_board, self.board_bottom_left, self.board_top_left
                    print self.video.aff_matrix
                else:

                    """ 
                    Update status of calibration flag and number of mouse
                    clicks
                    """

                    self.video.aff_flag = 2
                    self.video.mouse_click_id = 0
                    
                    """ Perform affine calibration with Least Squares """
                    
                    A = self.video.square_coord
                    b = np.transpose([np.append([], self.video.real_coord)])
                    A_t = np.transpose(A)

                    x_hat = np.dot((np.linalg.inv(np.dot(A_t,A))),np.dot(A_t,b))
                
                    """ Updates Status Label to inform calibration is done """ 
                    self.ui.rdoutStatus.setText("Waiting for input")

                    """ 
                    Uncomment to gether affine calibration matrix numbers 
                    on terminal
                    """ 
                    for i in range(2):
                        for j in range(3):
                            self.video.aff_matrix[i][j] = x_hat[3*i+j]

                    self.center_of_board = self.video.mouse_coord[0]
                    self.board_bottom_left = self.video.mouse_coord[1]
                    self.board_top_left = self.video.mouse_coord[2]

                    print self.video.aff_matrix

    def affine_cal(self):
        """ 
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label 
        """
        self.video.aff_flag = 1 
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d" 
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        self.video.loadCalibration()
        print "Load Camera Cal"

    def record_data(self, data, filename):
        f_data = open(filename, 'w')
        data = np.matrix(data)
        rows = data.shape[0]
        cols = data.shape[1]
        print data
        if rows == 0 or cols == 0:
            pass
        else:
            for i in range(rows):
                for j in range(cols):
                    if j == cols - 1:
                        f_data.write(str(data[i][j]))
                    if i < rows and i > 0:
                        f_data.write("\n")
                    break
                f_data.write(str(data[i][j]) + " ")
        f_data.close()

    def tr_initialize(self):
        self.state = "Teach"
        self.teach_waypoints = []
        self.smooth_waypoints = []
        self.cubic_waypoints = []
        self.waypoint_times = []
        self.teach_waypoint_counter = 0
        self.rex.max_torque = 0
        #self.rex.speed = 0
        self.rex.speed = 0
        self.rex.cmd_publish()
        print "Teach and Repeat"

    def tr_add_waypoint(self):
        if self.state == "Teach":
            # Grab current joint angles
            base_angle = self.rex.joint_angles_fb[0]
            shoulder_angle = self.rex.joint_angles_fb[1]
            elbow_angle = self.rex.joint_angles_fb[2]
            wrist_angle = self.rex.joint_angles_fb[3]

            # Save joint angles as waypoint
            waypoint = [base_angle, shoulder_angle, elbow_angle, wrist_angle]

            # If applying "smooth" function, add waypoints before and after to let
            # gripper go into and out of the waypoint
            if self.teach_smooth == True:
                if shoulder_angle > 0:
                    smooth_shoulder_angle = shoulder_angle - 15*D2R
                else:
                    smooth_shoulder_angle = shoulder_angle + 15*D2R
                smooth_waypoint = [base_angle, smooth_shoulder_angle, elbow_angle, wrist_angle]
                self.smooth_waypoints.append(smooth_waypoint)
                self.smooth_waypoints.append(waypoint)
                self.smooth_waypoints.append(smooth_waypoint)
                self.teach_waypoints.append(waypoint)
                print "Smoothing Path"
            else:
                self.teach_waypoints.append(waypoint)
                print "Not Smoothing Path"

        self.waypoint_times.append(int(time.time() * 1e6))
        #print self.teach_waypoints
            
        print "Add Waypoint"

    def tr_smooth_path(self):
        print "Smooth Path"
        self.state = "Cubic"
        self.cubic_waypoints = self.rex.cubic_smoothing(self.teach_waypoints, self.waypoint_times)
        #print self.cubic_waypoints

    def tr_playback(self):
        # Grab taught waypoint plan for playback
        plan = []

        # Record taught waypoints and their times
        self.record_data(self.teach_waypoints, "Teach_Waypoints.txt")
        self.record_data(self.waypoint_times, "Teach_Times.txt")

        # Record Smooth Waypoints if any
        self.record_data(self.smooth_waypoints, "Smooth_Waypoints.txt")

        # Record Cubic Waypoints and Velocities if any
        self.record_data(self.cubic_waypoints, "Cubic_Waypoints.txt")
        self.record_data(self.rex.cubic_velocities, "Cubic_Velocities.txt")
        self.record_data(self.rex.cubic_times, "Cubic_Times.txt")

        if self.teach_smooth == True:
            plan = self.smooth_waypoints
        else:
            plan = self.teach_waypoints

        # If taught plan was smoothed, replace plan with cubic version
        if self.state == "Cubic":
            print "Executing Cubic Path"
            plan = self.cubic_waypoints

        # Load plan to rexarm
        self.rex.load_plan(plan)

        # Use sliders to change torque
        self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
        self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0

        # Use sliders to change speed
        if self.state == "Cubic":
            self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")
            #self.rex.speed = self.ui.sldrSpeed.value()/100.0
            uispeed = self.ui.sldrSpeed.value()/100.0
            self.rex.speed = uispeed
        else:
            self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")
            #self.rex.speed = self.ui.sldrSpeed.value()/100.0
            uispeed = self.ui.sldrSpeed.value()/100.0
            self.rex.speed = uispeed

        # 
        self.state = "Playback"
        self.rex.plan_command()

        print "Playback"
        print self.rex.cubic_times

    def def_template(self):
        print "Define Template"
        # Initialization
        self.pixel_obs_matrix = []
        self.template_coords = []
        self.video.loadObstacles([])
        self.template_record = True
        self.template_status = True

    def CountContiguous(self, matrix, x, y):
        if (x < 1 or x > matrix.shape[0]-2 or  y < 1 or y > matrix.shape[1]-2):
            return 0
        else:
            if(matrix[x][y]<matrix[x-1][y] and matrix[x][y]<matrix[x][y-1] and matrix[x][y]<matrix[x][y+1] and matrix[x][y]<matrix[x+1][y] and matrix[x][y] < self.template_matching_threshold):
                return 1
            else:
                return 0

    def template_match(self):
        print "Template Match"

        # Start timing the template matching
        start_time = time.time()

        print self.pixel_obs_matrix
        print self.pixel_obs_matrix_gray
        self.template_status = True
        self.min_diff = 100000

        # Reset to newest video frame
        self.video.loadObstacles([])
        pixel_frame = cv2.cvtColor(self.video.currentFrame, cv2.COLOR_BGR2GRAY)
        self.num_of_obs = 0

        # Initilization
        self.affine_obs_location_list = []
        self.pixel_obs_location_list = []
        self.real_obs_loc = []
        self.real_num_of_obs = 0

        # Below coordinates are all in "Pixel Mouse Coordinates"
        # Step1. getting the vertices coordinate of the borders, we can get the coordinates when we do affine calibration
        self.board_size_x = abs(self.center_of_board[0] - self.board_top_left[0]) * 2 + 1
        self.board_size_x = int(self.board_size_x)
        self.board_size_y = abs(self.center_of_board[1] - self.board_top_left[1]) * 2 + 1  
        self.board_size_y = int(self.board_size_y)
        top_left_x = self.board_top_left[0]
        top_left_y = self.board_top_left[1]
        diff_matrix = np.zeros([(int(abs(self.board_size_x - self.obs_size_x + 1)) * 2), (int(abs(self.board_size_y - self.obs_size_y + 1))* 2)])

        mat_of_interest_max_y = top_left_y*2  + self.obs_size_y*2 + (self.board_size_y - self.obs_size_y + 1) * 2 - 1
        mat_of_interest_min_y = top_left_y*2
        mat_of_interest_max_x = top_left_x*2  + self.obs_size_x*2 + (self.board_size_x - self.obs_size_x + 1) * 2 - 1
        mat_of_interest_min_x = top_left_x*2
        domain_image = self.pixel_frame[mat_of_interest_min_y : mat_of_interest_max_y, mat_of_interest_min_x : mat_of_interest_max_x]

        cv2.imwrite("Domain_image.jpg", domain_image)

        # Step2. Comparing RGB values 
        filecount = 0
        for i in range((self.board_size_x - self.obs_size_x + 1) * 2):
            for j in range((self.board_size_y - self.obs_size_y + 1) * 2):
                
                filecount += 1
                matrix_of_interest = pixel_frame[top_left_y*2 + j : top_left_y*2 + j + self.obs_size_y*2 , top_left_x*2 + i : top_left_x*2 + i + self.obs_size_x*2]

                diff = np.subtract(matrix_of_interest, self.pixel_obs_matrix)
   
                Gray_diff = np.linalg.norm(diff)
                diff_matrix[i,j] = Gray_diff
        
        # Clear any previously stored obstacle data
        self.pixel_obs_loc = []
        self.real_obs_loc = []
        self.real_num_of_obs = 0

        # Find local minima in diff_matrix and store their pixel mouse coordinates
        for x in range(diff_matrix.shape[0]):
            for y in range(diff_matrix.shape[1]):
                if(self.CountContiguous(diff_matrix, x, y)) == 1:
                    pixel_mouse_obs_loc_x = int((2 * top_left_x + x + (self.obs_size_x * 2)/2) / 2.0)
                    pixel_mouse_obs_loc_y = int((2 * top_left_y + y + (self.obs_size_y * 2)/2) / 2.0)
                    print "Point: (%d, %d) Minima: %d" %(x, y, diff_matrix[x][y])
                    print "Pixel Coordinates:", pixel_mouse_obs_loc_x, pixel_mouse_obs_loc_y
                    self.pixel_obs_loc.append([pixel_mouse_obs_loc_x, pixel_mouse_obs_loc_y])

        # Calculate world coordinate waypoints

        print "Template Matching Complete"

        end_time = time.time()

        print "Time elapsed:", end_time - start_time

        arm_point = []
        for pixel_wpt in self.pixel_obs_loc:
            x_p = pixel_wpt[0]
            y_p = pixel_wpt[1]
            pixel_point = [[x_p], [y_p],[1]]
            world_point = np.dot(self.video.aff_matrix, pixel_point)
            x_w = world_point[0][0]
            y_w = world_point[1][0]

            if abs(x_w) < 2 and abs(y_w) < 2:
                arm_point = self.real_num_of_obs
                continue

            self.real_obs_loc.append([int(x_w), int(y_w)])
            self.real_num_of_obs += 1

        try:
            self.pixel_obs_loc.remove(arm_point)
        except:
            pass

        # Feed obstacles to video
        self.video.loadObstacles(self.pixel_obs_loc)


        print "obs size x : %d, y : %d " %(self.obs_size_x, self.obs_size_y)
        print "Pixel Mouse Location of obstacles, after  :", self.pixel_obs_loc
        print "World Mouse Location of obstacles, after  :", self.real_obs_loc
        print "Real number of obstacles : %d" %(self.real_num_of_obs)

        #print self.affine_obs_location_list


    def exec_path(self):
        if self.template_status == True:
            print "Execute Path for Targets"

            # For testing
            pose3 = [100, 100]
            pose2 = [0, -100]
            pose1 = [-75, -50]
            pose4 = [25, -78]

            # Iniiate poses to be the real obstacle locations as given by template matching
            poses = self.real_obs_loc
            #poses = [pose1, pose4, pose2, pose4]

            angle_list = []
            for i in range(self.real_num_of_obs):
                angle = math.atan2(self.real_obs_loc[i][1], self.real_obs_loc[i][0])
                if -1.0*np.pi <= angle <= -0.5*np.pi:
                    angle += 2.0*np.pi
                angle_list.append(angle)

            #print "before sort angle", angle_list
            for i in range(self.real_num_of_obs):
                j = i+1
                while j < self.real_num_of_obs:
                    if angle_list[i] > angle_list[j]:
                        temp = self.real_obs_loc[i]
                        self.real_obs_loc[i] = self.real_obs_loc[j]
                        self.real_obs_loc[j] = temp
                        temp2 = angle_list[i]
                        angle_list[i] = angle_list[j]
                        angle_list[j] = temp2
                    j += 1
        
            # Initiate list to keep track of waypoints
            new_poses = []
            for pose in poses:
                for j in range(24):
                    radius = (pose[0]**2 + (pose[1] - 10)**2)**(1/2.0)
                    print pose, radius
                    if radius < 100:
                        extra_pose = [pose[0], pose[1], abs(60-5*j), -90 -30]
                    elif radius > 190:
                        extra_pose = [pose[0], pose[1], abs(60-5*j), -90 + 30]
                    else:
                        extra_pose = [pose[0], pose[1], abs(60-5*j), -90 + 0]

                    new_poses.append(extra_pose)

            angles = []

            for pose in new_poses:

                # Append z location to waypoint. Note it is assumed arm goes all the way down
                # For testing, we are currently having the end endeffector perpendicular to ground

                print "Desired pose:", pose
                desired_pose = pose

                # Grab inverse kinematics joint waypoint if joint configuration is possible
                waypoint = self.rex.rexarm_IK(desired_pose)
                
                print "IK waypoint:", waypoint

                # If joint configuration is possible add to list of angles
                if waypoint != None:
                    base_angle = waypoint[0]# - 1*D2R
                    shoulder_angle = waypoint[1]
                    elbow_angle = waypoint[2]
                    wrist_angle = waypoint[3]

                    angles.append(waypoint)
                        # print "Not Smoothing Path"
                # If configuration is not possible print message and skip       
                else:
                    print "Configuration not found!"
            # Return arm to vertical standing position
            angles.append([0,0,0,0])

            print "Waypoints to command:", angles

            # Load plan and command path
            self.rex.load_plan(angles)
            self.rex.plan_command()

        else:
            print "Execute Fun Path"
            num_of_wpt_each_cycle = 30
            num_of_wpt_fun = 11.5*num_of_wpt_each_cycle
            fun_path = []

            maze_height = 324 # mm
            maze_outer_rad = 170 # mm

            # Add code for maze path here! Replace the fun path!
            maze_matrix = getMaze("maze01", 324, 324)
            decode_matrix = decodeMaze(maze_matrix)
            start_idx, start_idy = findStart(maze_matrix)
            sim_solution = getSolution(start_idx, start_idy, maze_matrix)
            shifted_solution = shiftList(sim_solution, 324/2)
            cell_width = rad2epsilon(324, maze_outer_rad)
            world_solution = sim2worldList(shifted_solution, cell_width)
            indices = []

            # i = 15
            # while i < num_of_wpt_fun:
            #     time = i * 2*np.pi/num_of_wpt_fun
            #     x_fun = 149.5*math.cos(time) + 2.6 + 38*math.cos(12.0*time)
            #     y_fun = 149.5*math.sin(time) + 38*math.sin(12.0*time)
            #     z_fun = 50.0 + 149.5*math.sin(7.15*D2R)*math.sin(time) + 38*math.sin(5.14*D2R)*math.sin(12.0*time)
            #     fun_path.append([y_fun, -x_fun, z_fun, -90])
            #     i += 1

            # Rotate Points to avoid p/m pi boundary
            rot_angle = -90*D2R
            rot_matrix01 = [[math.cos(rot_angle), -math.sin(rot_angle)], [math.sin(rot_angle), math.cos(rot_angle)]]
            rot_angle = -10*D2R
            rot_matrix02 = [[math.cos(rot_angle), -math.sin(rot_angle)], [math.sin(rot_angle), math.cos(rot_angle)]]

            for point in world_solution:
                x_fun = -point[0]
                y_fun = point[1]
                z_fun = maze_height
                point = np.matmul(rot_matrix01, [[x_fun],[y_fun]])
                point = np.matmul(rot_matrix02, [-point[0], point[1]])
                x_fun = point[0]
                y_fun = point[1]
                fun_path.append([x_fun, y_fun, z_fun, 90]) # End Effector facing up!
                print x_fun, y_fun, z_fun

            angles = []

            index = 0

            for pose in fun_path:

                # Append z location to waypoint. Note it is assumed arm goes all the way down
                # For testing, we are currently having the end endeffector perpendicular to ground

                # print "Desired pose:", pose
                desired_pose = pose

                # Grab inverse kinematics joint waypoint if joint configuration is possible
                waypoint = self.rex.rexarm_IK(desired_pose)
                
                # print "IK waypoint:", waypoint

                # If joint configuration is possible add to list of angles
                if waypoint != None:
                    base_angle = waypoint[0]# - 1*D2R
                    shoulder_angle = waypoint[1]
                    elbow_angle = waypoint[2]
                    wrist_angle = waypoint[3]
                    angles.append(waypoint)
                    indices.append(index)
                        # print "Not Smoothing Path"
                # If configuration is not possible print message and skip       
                else:
                    print "Configuration not found!"

                index += 1
            # Return arm to vertical standing position
            # angles.append(angles[-1])

            count = 0
            for psi in angles:
                FK_pose = self.rex.rexarm_FK(psi, 4)
                print "IK waypoint:", [psi[0], psi[1], psi[2], psi[3]]
                print "FK waypoint:", FK_pose
                print "DS waypoint:", fun_path[indices[count]]
                count += 1
            
            #print angles

            writer = csv.writer(open("fun_path_joint_angles_command.csv", 'w'))
            writer.writerows(angles)

            # print "Waypoints to command:", angles

            # Load plan and command path
            self.rex.load_plan(angles)
            self.rex.plan_command()

def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())
 
if __name__ == '__main__':
    main()
