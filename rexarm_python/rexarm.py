"""
Rexarm.py

Used to command the dynamixels corresponding to the arm base, shoulder, elbow, wrist. Creates a rexarm object with predefined joint angles,
hence the arm begins vertically up, speeds, and motor torques. In addition, it initiates variables to be used later for calculating motor
feedback, forward kinematics, path planning, and DH tables.

Functions: 
cmd_publish(self): -> Commands the arm using the lcm.
get_feedback(self): -> Updates the joint angle feedback from the lcm and calculates the end effector position.
feedback_handler(self, channel, data): -> LCM handler used to decode lcm dynamixel statuses messages.
clamp(self): -> Used to limit the commanded joint angles within the physical limits of each joint.
load_plan(self, waypoints): -> Used to load the list of waypoints to be commanded.
load_times(self, times): -> Used to load the times of plan if needed for cubic smoothing.
plan_command(self): -> Using the currently stored plan, this function commands each waypoint in the plan.
cubic_smoothing(self, plan, times): -> Given a plan of waypoints and their respective times, using a cubic approximation, 
                                       additional waypoints are added to smooth the path.
rexarm_FK(self, angles, link): -> Given a designated link, it returns the position of the link in the world coordinates.
rexarm_IK(self, pose): -> Given the desired position of the end effector, this function calculates the elbow up joint space configuration of the arm that is
                          necessary to achieve the desried position, if possible. If not possible, it returns None.
"""

import lcm
import time
import math
import csv
import numpy as np

from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t

PI = np.pi
D2R = PI/180.0
R2D = 180.0/PI
ANGLE_TOL = 2*PI/180.0 


""" Rexarm Class """
class Rexarm():
    def __init__(self):

        """ Commanded Values """
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians
        # you SHOULD change this to contorl each joint speed separately 
        #self.speed = 0.5                         # 0 to 1
        self.speed = 0.5
        self.max_torque = 0.5                    # 0 to 1

        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius
        self.time_fb = 0

        """ Forward Kinematics """
        self.forward_x = 0
        self.forward_y = 0
        self.forward_z = 0

        """ Cubic Smoothing"""
        self.v_init = 0
        self.v_final = 0
        self.dt = 0
        self.dq = 1e-3

        """ Clamp Values"""
        self.wrist_min = -126 * D2R
        self.wrist_max = 128 * D2R
        self.elbow_min = -123 * D2R
        self.elbow_max = 122 * D2R
        self.shoulder_min = -123 * D2R
        self.shoulder_max = 121 * D2R
        self.base_min = -179 * D2R
        self.base_max = 180 * D2R

        """Length of Links"""
        self.l_wrist = 108 #mm
        self.l_elbow = 100 #mm
        self.l_shoulder = 100 #mm
        self.z_offset = 118 #mm

        """Gripper Angle"""
        self.gripper_angle = -90 * D2R

        """ Plan - TO BE USED LATER """
        self.plan = []
        self.plan_status = 0
        self.wpt_number = 0
        self.wpt_total = 0
        self.wpt_treshold = 0.10
        self.plan_times = []
        self.cubic_plan = []
        self.cubic_times = []
        self.threshold = [0.0, 0.0, 0.0, 0.0]
        self.thresweight = 0.05
        self.cubic_velocities = []

        """ DH Tables """
        self.alpha = [90*D2R, 0, 0, 0]
        self.d = [118, 0, 0, 0]
        self.a = [0, 100, 100, 109]
        self.theta = [ 0, 0, 0, 0]

        dt_table_0 = np.zeros((4,4))
        dt_table_1 = np.zeros((4,4))
        dt_table_2 = np.zeros((4,4))
        dt_table_3 = np.zeros((4,4))
        self.dh_tables = [dt_table_0, dt_table_1, dt_table_2, dt_table_3]

        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)

    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        msg = dynamixel_command_list_t()
        msg.len = 4
        self.clamp()
        for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            cmd.position_radians = self.joint_angles[i]
            # you SHOULD change this to contorl each joint speed separately 
            #print self.speed
            cmd.speed = self.speed
            cmd.max_torque = self.max_torque
            #print cmd.position_radians
            msg.commands.append(cmd)
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)
        theta_fb = self.joint_angles_fb
        self.theta = [90*D2R + theta_fb[0], 90*D2R - theta_fb[1], -theta_fb[2], -theta_fb[3]]
        for i in range(4):
            alpha = self.alpha[i]
            d = self.d[i]
            a = self.a[i]
            theta = self.theta[i]
            self.dh_tables[i] = np.array([  [math.cos(theta), -math.sin(theta)*math.cos(alpha), math.sin(theta)*math.sin(alpha), a*math.cos(theta) ],
                                            [ math.sin(theta), math.cos(theta)*math.cos(alpha), -math.cos(theta)*math.sin(alpha), a*math.sin(theta)],
                                            [0, math.sin(alpha), math.cos(alpha), d], [ 0, 0, 0, 1] ])

        D = np.dot(self.dh_tables[0], np.dot(self.dh_tables[1], np.dot(self.dh_tables[2], self.dh_tables[3])))

        self.forward_x = D[0][3]
        self.forward_y = D[1][3]
        self.forward_z = D[2][3]

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians 
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature

    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        TO DO: IMPLEMENT SUCH FUNCTION
        """

        # Clamping the base
        if self.joint_angles[0] < self.base_min:
            self.joint_angles[0] = self.base_min
        elif self.joint_angles[0] > self.base_max:
            self.joint_angles[0] = self.base_max

        # Clamping Shoulder
        if self.joint_angles[1] < self.shoulder_min:
            self.joint_angles[1] = self.shoulder_min
        elif self.joint_angles[1] > self.shoulder_max:
            self.joint_angles[1] = self.shoulder_max

        # Clamping Elbow
        if self.joint_angles[2] < self.elbow_min:
            self.joint_angles[2] = self.elbow_min
        elif self.joint_angles[2] > self.elbow_max:
            self.joint_angles[2] = self.elbow_max

        # Clamping Wrist
        if self.joint_angles[3] < self.wrist_min:
            self.joint_angles[3] = self.wrist_min
        elif self.joint_angles[3] > self.wrist_max:
            self.joint_angles[3] = self.wrist_max

    def load_plan(self, waypoints):
        """ Set the current plan to follow. """

        self.plan = waypoints
        self.wpt_number = 0
        self.wpt_total = len(waypoints)

    def load_times(self, times):

        self.plan_times = times

    def plan_command(self):
        """ Command waypoints - TO BE ADDED LATER """

        if self.wpt_total == 0:
            print "Plan is empty"
    
        else:
            feedback_angles = []
            f1 = open("Feedback_Waypoints.txt", "w")
            f2 = open("Feedback_Times.txt", "w")
            f3 = open("End_Effector.txt", "w")

            while(True):
                self.get_feedback()

                # Select waypoint
                waypoint = self.plan[self.wpt_number]

                self.joint_angles = waypoint

                # Import threshold for teach and repeat waypoints
                if self.speed < 0.10:
                    self.wpt_treshold = 0.06
                else:
                    self.wpt_treshold = 0.12
                
                threshold = self.wpt_treshold

                # Import feedback from lcm
                feedback = self.joint_angles_fb
                feedback_time = int(time.time() * 1e6)

                # Write feedback waypoints and times
                f1.write(str(feedback) + "\n")
                f2.write(str(feedback_time) + "\n")
                f3.write(str([self.forward_x, self.forward_y, self.forward_z]))
                f3.write("\n")

                # Calculate joint errors between feedback and desired
                base_error = abs(waypoint[0] - feedback[0])
                shoulder_error = abs(waypoint[1] - feedback[1])
                elbow_error = abs(waypoint[2] - feedback[2])
                wrist_error = abs(waypoint[3] - feedback[3])

                errors = (base_error, shoulder_error, elbow_error, wrist_error)

                print "Error:", errors, "Position:", self.rexarm_FK(feedback, 4)
                print "Threshold:", threshold, "Waypoint:", waypoint
                
                #print self.wpt_number, base_error, shoulder_error, elbow_error, wrist_error

                if base_error < threshold and shoulder_error < threshold and elbow_error < threshold and wrist_error < threshold:
                    self.wpt_number += 1
                    feedback_angles.append([feedback[0], feedback[1], feedback[2], feedback[3]])

                if self.wpt_number == self.wpt_total:
                    self.wpt_number = 0
                    break

                self.cmd_publish()
            #print "feedback_angles", feedback_angles
            writer = csv.writer(open("fun_path_joint_angles_feedback.csv", 'w'))
            writer.writerows(feedback_angles)

            f1.close()
            f2.close()
            f3.close()

    def cubic_smoothing(self, plan, times):

        # Clear any previous cubic plans
        self.cubic_plan = []
        self.cubic_times = []
        self.cubic_velocities = []
        total_time = 0

        for i in range(len(plan) - 1):
            # Get data for current and next waypoint
            waypoint_0 = plan[i]
            waypoint_f = plan[i+1]
            t0 = times[i] 
            tf = times[i+1] 

            # Reset the cubic smoothing constants
            a0 = [0, 0, 0, 0]
            a1 = [0, 0, 0, 0]
            a2 = [0, 0, 0, 0]
            a3 = [0, 0, 0, 0]
            dt = [0, 0, 0, 0]

            # Calculate the associated a_i values and the associated dts
            for j in range(len(waypoint_0)):
                q0 = waypoint_0[j]
                qf = waypoint_f[j]
                v0 = self.v_init
                vf = self.v_final
                a0[j] = q0
                a1[j] = v0
                a2[j] = (3*(qf - q0) - (2*v0 + vf)*(tf - t0))/(tf - t0)**2
                a3[j] = (2*(q0 - qf) + (v0 +vf) * (tf - t0))/(tf - t0)**3
                if abs(qf - q0) < 1e-8:
                    dt[j] = tf - t0 + 1
                else:
                    dt[j] = (tf - t0)/float(abs(qf - q0)/self.dq)

                self.threshold[j] = (qf-q0)*self.thresweight

            dt = max(dt)
            curr_time = 0

            while curr_time < tf - t0:
                waypoint = [0, 0, 0, 0]
                velocity = [0, 0, 0, 0]
                for k  in range(len(waypoint_0)):
                    waypoint[k] = a0[k] + a1[k]*(curr_time) + a2[k]*(curr_time)**2 + a3[k]*(curr_time)**3
                    velocity[k] = a1[k] + 2*a2[k]*(curr_time) + 3*a3[k]*(curr_time)**2
                    velocity[k] = velocity[k]*1e6
                self.cubic_plan.append(waypoint)
                self.cubic_velocities.append(velocity)
                self.cubic_times.append(total_time)
                curr_time = curr_time + dt
                total_time = total_time + dt

            total_time = tf - times[0]

            #print tf - times[0], total_time - dt
                #print (times[-1] - times[0]), total_time

        self.cubic_plan.append(plan[-1])
        self.cubic_velocities.append([0.0, 0.0, 0.0, 0.0])
        self.cubic_times.append(times[-1] - times[0])
        print (times[-1] - times[0]), total_time

        return self.cubic_plan

    def rexarm_FK(self, angles, link):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for returns a 4-tuple 
        (x, y, z, phi) representing the pose of the desired link. 

        (Link 1 = Shoulder, Link 2 = Elbow, Link 3 = Wrist, Link 4 = End Effector)
        """

        # Prepare DH tables for given angles
        dt_table_0 = np.zeros((4,4))
        dt_table_1 = np.zeros((4,4))
        dt_table_2 = np.zeros((4,4))
        dt_table_3 = np.zeros((4,4))
        dh_tables = [dt_table_0, dt_table_1, dt_table_2, dt_table_3]

        # Calculate DH tables for given angles
        psi = [90*D2R + angles[0], 90*D2R - angles[1], -angles[2], -angles[3]]
        # print "Self Angles:", self.theta
        # print "Inpt Angles:", psi
        #theta = angles
        for i in range(4):
            alpha = self.alpha[i]
            d = self.d[i]
            a = self.a[i]
            theta = psi[i]
            dh_tables[i] = np.array([  [math.cos(theta), -math.sin(theta)*math.cos(alpha), math.sin(theta)*math.sin(alpha), a*math.cos(theta) ],
                                            [ math.sin(theta), math.cos(theta)*math.cos(alpha), -math.cos(theta)*math.sin(alpha), a*math.sin(theta)],
                                            [0, math.sin(alpha), math.cos(alpha), d], [ 0, 0, 0, 1] ])

        link_table = np.eye(4) 

        # Multiply the tables to find the pose of the desired link
        for i in range(link):
            link_table = np.dot(link_table, dh_tables[i])


        # print link_table
        D = np.dot(dh_tables[0], np.dot(dh_tables[1], np.dot(dh_tables[2], dh_tables[3])))
        # print D

        # Find and return pose of desired link
        x = link_table[0,3]
        y = link_table[1,3]
        z = link_table[2,3]

        phi = math.atan(y/x)

        link_pose = [x, y , z, phi]

        return link_pose
    	
    def rexarm_IK(self, pose):
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        """

        # Make zero for elbow down configuration, one otherwise
        cfg = 0

        [x_target, y_target, z_target, phi_target] = pose

        z_target = z_target - self.z_offset
        print z_target

        # Initiate desired gripper orientation in radians
        self.gripper_angle = phi_target * D2R
        print self.gripper_angle

        # Initiate Joint Angles
        base_angle = 0
        shoulder_angle = 0
        elbow_angle = 0
        wrist_angle = 0

        # Calculate Base Angle
        base_angle = math.atan2(y_target,x_target) + 90*D2R
        print "Base Angle:", base_angle*R2D

        # Calcuate projection of target point onto x-y plane
        w_target = math.sqrt(x_target**2 + y_target**2)

        # Calculate wrist location in w-z plane
        w_wrist = w_target - self.l_wrist*math.cos(self.gripper_angle)
        z_wrist = z_target - self.l_wrist*math.sin(self.gripper_angle)

        # print "W_WRIST:", w_wrist, y_target, "Z_WRIST", z_wrist, z_target

        print "Wrist Height:", z_wrist

        # Calculate shoulder to wrist informaation in w-z plane
        l_shoulder_to_wrist = math.sqrt(w_wrist**2 + (z_wrist)**2)
        shoulder_to_wrist_angle = math.atan2(z_wrist,w_wrist)

        # print w_target, l_shoulder_to_wrist

        # If configuration is not possible return None, otherwise continue.
        if l_shoulder_to_wrist > self.l_shoulder + self.l_elbow:
            print "Radius out of bounds"
            return None

        # If elbow down, calculate the appropriate shoulder angle in the w-z plane.
       
        stuff = ((l_shoulder_to_wrist**2 - self.l_shoulder**2 - self.l_elbow**2) / (2.0*self.l_shoulder*self.l_elbow))
        
        elbow_angle = -1000

        if cfg == 0: # Elbow Down
            elbow_angle = math.atan2(math.sqrt(1 - stuff**2), stuff)
        elif cfg == 1: # Elbow Up
            elbow_angle = math.atan2(-math.sqrt(1 - stuff**2), stuff)
        else:
            throw("Invalid elbow configuration")

        # Calculate the elbow position with respect to the shoulder in the w-z plane.
        # w_elbow = self.l_shoulder * math.cos(shoulder_angle)
        # z_elbow = self.l_shoulder * math.sin(shoulder_angle)

        # Calculate the appropriate elbow angle in the w-z plane.
        #elbow_angle = (math.atan2((z_wrist - z_elbow),(w_wrist - w_elbow)) - shoulder_angle)

        shoulder_to_elbow_angle = math.atan2(self.l_elbow*math.sin(elbow_angle), self.l_shoulder + self.l_elbow*math.cos(elbow_angle))
        shoulder_angle = shoulder_to_wrist_angle + shoulder_to_elbow_angle
        shoulder_angle = 90*D2R - shoulder_angle

        #print (R2D*base_angle, R2D*shoulder_angle, R2D*elbow_angle, R2D*wrist_angle)

        #base_angle = base_angle - 90*D2R

         # Clamping the base
        if base_angle < self.base_min:
            base_angle = 360*D2R - abs(base_angle)
        elif base_angle > self.base_max:
            base_angle = -(360*D2R - abs(base_angle))

        # Calculate the appropriate wrist angle, given the desired orientation of the gripper.
        wrist_angle = 90*D2R - self.gripper_angle - shoulder_angle - elbow_angle

        print (R2D*base_angle, R2D*shoulder_angle, R2D*elbow_angle, R2D*wrist_angle)

        # Clamping Shoulder
        if shoulder_angle < self.shoulder_min:
            print "Shoulder Out of Bounds"
            return None
        elif shoulder_angle > self.shoulder_max:
            print "Shoulder Out of Bounds"
            return None

        # Clamping Elbow
        if elbow_angle < self.elbow_min:
            print "Elbow Out of Bounds"
            return None
        elif elbow_angle > self.elbow_max:
            print "Elbow Out of Bounds"
            return None

        # Clamping Wrist
        if wrist_angle < self.wrist_min:
            print "Wrist Out of Bounds"
            return None
        elif wrist_angle > self.wrist_max:
            print "Wrist Out of Bounds"
            return None

        angles = [base_angle, shoulder_angle, elbow_angle, wrist_angle]

        # if self.rexarm_FK(angles, 4) < 0:
        #     return None

        return angles
        