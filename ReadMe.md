Filename: ReadMe.mdL
Author: Cosme Ochoa

Description: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Provides a short description of the relevant project files regarding the controlling
of the robot arm and the interface used to execute the maze (and other modes).

Startup: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

First make sure that all code is compiled correctly. Normally, this will need to 
be completed only one per the entire run of a session with the Rexarm

A Makefile is provided to compile the C code and all necessary inter-communication
files (LCM types). Simply run "make" or "make all" or "make clean" as deemed appropriate
from the root project directory: /armlab. Given that most of the main code for our
algorithms is in the Python scripts you only need to run make once.

To communicate to the Rexarm its required that two programs run locally on the 
home computer: a low level serial interace (rexarm_driver) and the control station 
use to interact with the arm (control_station.py).

To start up the rexarm drivers simply start the rexarm drivers as follows. (Note
it assumed you are doing this from inside the project directory)

command: sudo ./bin/rexarm_driver

You should see whether the drivers are being recognized and their current status.
If everything is working properly DO NOT CLOSE THIS TERMINAL WINDOW.

Open a second terminal window and once again head to the project directory. Once
there run the following command to load up the control station GUI.

command: python /rexarm_python/control_station.py

GUI Interface: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The control station GUI has been equiped with a set of buttons and sliders used
to control the actions of the rexarm joints. Below we provide a descrition of what
each of the buttons and liders do.

Top Left:

Joint Coordinates, World Coordinates: Approximate values for the joint values of 
the rexarm and the end effector posion with respect to its home configuration.

Note, these values were calibrated for another arm most likely, so they might not 
be exact, but they do provide a enough for qualitative analysis.

Top Right:

Maze Window: Currently this is a blank canvas for a future widget yet to be coded.
The widget would show the maze being solved along with the end effector's position
as it attempts to trace the solution

Bottom Left:

Load Camera Calibration: Used to calibrate an overhead camera. (N/A)

Perform Affine Calibation: Used to map camera pixel cooridnates to world. (N/A)

Begin Teach and Repeat: Sets the motors of the robot arm to idle. This allows the 
user to phycially move the arm in pattern to be repeated later.

Add Waypoint: As one moves the arm during the teaching process, click this button
to add waypoints for the arm to repeat.

Load Plan: Ends the teaching session and loads the currently accumulated list of
waypoints and places them on the execution queue.

Smooth Plan: If clicked, this grabs the current list of waypoints on the execution
queue and replaces the path with cubic spline smoothed path (see Spong book)

Save and Playback: This button takes the current list of waypoints on the queue and
attempts to execute the path with the torque and speed set by the GUI when clicked.

Define Template: Used to define an object in the camera view as an obstacle. (N/A)

Locate Targets: Finds all objects in the camera field of view that match the defined
template within some tolerance. (N/A)

Execute Path: Executes some predefined path. Currently this button is set to load
csv files defining the maze (currently found in /armlab/rexarm_python/Scenes), finds
the solution, and then executes it

Bottom Right:

Joint Sliders: Set of sliders to change the joint angles for the rexarm base,
shoulder, elbow, and wrist as long as the configuration is valid.

Torque, Speed Sliders: Used to set the motor torques and speeds. If you see that
the arm is getting stuck increase these values accordingly.

Functions: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

rexarm_driver: C excutable used to start the communication link between the arm's
motors and control station interface

Location: /armlabm/bin

control_station.py: Python script file used to send commands to the robot arm

Location: /armlab/rexarm_python/control_station.py
